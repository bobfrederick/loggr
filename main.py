from dearpygui.core import *
from dearpygui.simple import *
#from dearpygui import demo
import os
import sys
import atexit
import json
from folderparse import FOLDER_CHILD, walk, build_dict, PROGRESS
from excelwriter import write_folder_data_to_excel
from vpn import vpn_status
import time
import shutil

__author__ = 'bfrederick@rios.com'
__version__ = '0.0.3'

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

FOLDER_DATA = None
CURRENT_FOLDER = None # current folder path saved
APPDATA_FILE = resource_path("app.json")
APPDATA = {"paths":{}}
CURR_DIR = os.getcwd()
LOCAL_APP_DATA_FOLDER = os.path.join(os.getenv('APPDATA'),r"RIOS Tools\Loggr")
LOCAL_APP_DATA_FILE = os.path.join(os.getenv('APPDATA'),r"RIOS Tools\Loggr\app.json")
# Set to either 'development' or 'production' to determine where appdata.json will be written.
BUILD = 'production'

# check for json app file and load existing paths if they exist
def load_saved_paths():
    global APPDATA_FILE
    global APPDATA
    if os.path.exists(APPDATA_FILE):
        try:
            app_data = open(APPDATA_FILE)
            APPDATA = json.load(app_data)
        except ValueError as e:
            print(f"Couldn't load existing filepaths from {APPDATA_FILE}...")

# create app data folder on first run
def initalize_app_data():
    """ Initalize the appdata into the local appdata folder for writing permissions"""
    global APPDATA_FILE
    global LOCAL_APP_DATA_FOLDER
    global LOCAL_APP_DATA_FILE
    if os.path.exists(LOCAL_APP_DATA_FILE) and BUILD == "production" :
        APPDATA_FILE = LOCAL_APP_DATA_FILE
        load_saved_paths()
    elif not os.path.exists(LOCAL_APP_DATA_FILE) and BUILD == "production":
        os.makedirs(LOCAL_APP_DATA_FOLDER)
        shutil.copyfile(APPDATA_FILE, LOCAL_APP_DATA_FILE)
        APPDATA_FILE = LOCAL_APP_DATA_FILE
    elif BUILD == "development":
        print("Working in Development Mode | App Data will be saved relative to main app")

initalize_app_data()

# write app data to file
def write_app_data():
    """ Add appdata to appdata.json file"""
    global APPDATA
    print(f"Writing app data into {APPDATA_FILE}")
    # add the paths to a json file
    with open(APPDATA_FILE, 'w') as f:
        json.dump(APPDATA, f)

# app exit handler
def exit_handler():
    """ handle exit commands here"""
    write_app_data()
    
# register the app exit handler
atexit.register(exit_handler)

def save_path(path):
    """ save a search path to json file """
    global APPDATA
    if path in APPDATA['paths'].keys():
        pass
    else:
        APPDATA['paths'][path] = []

# clear the current path from the searchbar
def clear_current_path():
    """ clears the current path from the search bar """
    set_value("StudyFolderPath", "")
    configure_item("StudyFolderPath", hint="Copy path to your folder here. Press Enter or Preview to view the log")

def clear_history(sender, data):
    """ clear saved paths history """
    global APPDATA
    # clear anything in memory
    open(APPDATA_FILE, 'w+b').close()
    # remove UI components
    configure_item("ClearFolderHistory", show=False)
    if len(APPDATA['paths'].keys()) != 0: 
        for button in APPDATA['paths'].keys():
            if does_item_exist(button):
                try:
                    delete_item(button)
                except:
                    pass
        APPDATA = {"paths":{}}

def get_folder_data(folderpath):
    """ return data from folder path """
    dirs = list(walk(folderpath, 2))
    ## check for invalid directory
    if len(dirs) >=1:
        data =  build_dict(dirs)
        return data
    else:
        return None
 
def diff_data(new_data, old_data):
    """ Compares two nested lists. Returns only unique lists or None. 
        Tests each new list element with each old list element, finding subsets.
        Finds the set difference between original new lists and subsets list.
    """
    temp1 = []
    temp2 = []
    for i in new_data:
        # convert each list to a tuple for use in later set comparison
        temp1.append(tuple(i))
        for j in old_data:
            if i == j:
                temp2.append(tuple(i))

    # get the set difference between all the subset elements and the original list
    added = set(temp1).difference(temp2)
    unchanged = set(temp1).intersection(temp2)
    exact_match = new_data == old_data
    removed = set([tuple(k) for k in old_data]).difference(temp1)
    if exact_match:
        return None
    else:
        return (new_data, list(added), list(unchanged), list(removed))

def cache_table_data(folderpath, data):
    """ Saves the folder data for caching in an APPDATA dictionary that. 
        This method does not save the data to disk. 

    Parameters
    ----------
    folderpath : a file-like string path to a folderpath
    data : 
    
    """
    APPDATA['paths'][folderpath]= {'mod_date' : time.strftime('%m.%d.%y %I:%M %p'), 'data': data}
   
def update_table(name, folderpath, data):
    """ Update the preview table """
    # check for existing data if it's been saved in the cache
    try: 
        original_data = APPDATA['paths'][folderpath]['data']
    except:
        original_data = None
    # transform new data from dict into nested list
    new_data =  [[row["group"],row["study_no"], row["study_name"], row["date_created"]] for row in data]

    """ #DEBUGGING
    print("-----------------------------OLD DATA---------------")
    print(original_data)
    print("-----------------------------NEW DATA---------------")
    print(new_data) """
    
    # check if there is original data first
    if original_data:
        # check the differences and load them otherwise load the cached data
        diff = diff_data(new_data, original_data)
        if diff:
            """ print("--------------------DIFF----------------")
            print("CURRENT------------")
            print(diff[0])
            print("Found differences...")
            print("ADDED ---------------")
            print(diff[1])
            print("REMOVED ---------------")
            print(diff[3]) """
            # set the existing table
            set_table_data(name, diff[0])
            # set the added table
            set_table_data("AddedTable", diff[1])
            # set the removed table
            set_table_data("RemovedTable", diff[3])
            cache_table_data(folderpath,diff[0])
            last_date = APPDATA['paths'][folderpath]['mod_date']
            set_value("Preview Changes",
                      f"{len(diff[1])+len(diff[3])} Changes | {len(diff[1])} Added | {len(diff[3])} Removed | since {last_date}")
        else:
            #print("No Changes.. loading data from cache..")
            # set UI element to show cached date
            last_date = APPDATA['paths'][folderpath]['mod_date']
            set_value("Preview Changes", f"No changes since {last_date}")
            set_table_data(name, APPDATA['paths'][folderpath]['data'])
            write_app_data()
    else:
        # if no original data just load the new data
        set_table_data(name, new_data)
        # save data to the app data cache for the first time.
        cache_table_data(folderpath,new_data)
        # write the cache to the file
        write_app_data() 

def table_action(sender, data):
    """ open file on path selection """
    #log_debug(f"Table Called: {sender}")
    coord_list = get_table_selections("ExistingTable")
    #log_debug(f"Selected Cells (coordinates): {coord_list}")
    names = []
    for coordinates in coord_list:
        names.append(get_table_item("ExistingTable", coordinates[0], coordinates[1]))
    #log_debug(names)

def set_folder(sender, data):
    """ sets folder path for processing """
    if data and os.path.exists(data):
        global CURRENT_FOLDER
        global FOLDER_DATA
        CURRENT_FOLDER = data
        # get the data if the folder doesn't match the required folder template structure let user know
        FOLDER_DATA = get_folder_data(data)
        if FOLDER_DATA:
            set_value("StudyFolderPath",data)
            update_table("ExistingTable",CURRENT_FOLDER,FOLDER_DATA)
            # add path to history for next app launch
            save_path(data)
            # reset the progress bar back to zero.
            set_value("ProgressBar", 0)
            configure_item("ProgressBar", overlay=f"Total in current folder: {len(FOLDER_DATA)-1}")
            set_item_color("ProgressBar", color=(71,71,71), style=mvGuiCol_PlotHistogram)
        else:
            set_value("StudyFolderPath", "")
            configure_item("StudyFolderPath", hint="This directory is not valid.")
            set_value("ProgressBar", 0 )
            configure_item("ProgressBar", overlay="")
            set_item_color("ProgressBar", color=(71,71,71), style=mvGuiCol_PlotHistogram)
    else:
        set_value("StudyFolderPath", "")
        configure_item("StudyFolderPath", hint="Please input an existing, valid directory or check access to the drive.")
        set_value("ProgressBar", 0 )
        configure_item("ProgressBar", overlay="")
        set_item_color("ProgressBar", color=(71,71,71), style=mvGuiCol_PlotHistogram)
    
#TODO check that table has data to export!   
def export_data():
    """ Write existing data to an excel file 
    
    Global Parameters
    ----------
    CURRENT_FOLDER : string, file-like path to directory for saving the file.
    FOLDER_DATA : dictionary, data to write to the excel file.
    
    """
    global CURRENT_FOLDER
    global FOLDER_DATA

    if CURRENT_FOLDER and FOLDER_DATA:
        configure_item("ProgressBar", overlay=f"Exporting log...")
        set_item_color("ProgressBar", color=(24,115,96), style=mvGuiCol_PlotHistogram)
        status = write_folder_data_to_excel(CURRENT_FOLDER, FOLDER_DATA)
        if status == True:
            set_value("ProgressBar", 100 )
            configure_item("ProgressBar", overlay=f"Log exported here: {CURRENT_FOLDER}")
        else:
            set_value("ProgressBar", 0 )
            configure_item("ProgressBar", overlay=f"Error exporting: {status}")
            set_item_color("ProgressBar", color=(197,14,46), style=mvGuiCol_PlotHistogram)
    else:
        status = None
        configure_item("ProgressBar", overlay=f"Run Preview on a valid folder before exporting.")
        

    return status

# main window setup and font import
add_additional_font("fonts\Roboto-Light.ttf", 16)
set_theme("Dark Grey")
set_main_window_size(width=900,height=600)
set_main_window_resizable(False)
set_main_window_title(title="rios | Logger v 0.0.3")

# setup left panel : navigation
with window("LeftNav",
            no_title_bar = True,
            autosize=False, 
            no_resize=True,
            no_move=True,
            x_pos = 0,
            y_pos = 0, 
            width = 220,
            height = get_main_window_size()[1]-80,
            ):
    #TODO Add callbacks for buttons. 
    add_image("LogoImg", value='resources\StudyTrackerLogo.png')
    add_spacing(count=5)
    add_image_button("Preview", value='resources\Preview.png',
                     callback= set_folder,
                     callback_data= lambda: get_value("StudyFolderPath"),
                     background_color=[255,255,255,1]
                    )
    add_image_button("Export",value='resources\Export.png',
                     callback= export_data,
                    )


set_style_item_spacing(3.00, 4.00)
set_style_frame_padding(4.50,4.00)
set_style_window_padding(20.00,8.00)


set_item_color("LeftNav", mvGuiCol_WindowBg, [51,54,56,138])
set_item_color("LeftNav", color=[0,0,0,0], style=mvGuiCol_Button)

# setup right panel : search bar
with window("Search",
            no_title_bar = True,
            autosize = False,
            no_resize=True,
            no_move=True,
            x_pos = 220,
            y_pos = 0,
            width = 660,
            height = 80,
            ):
    add_spacing(count=3)
    add_text("Current Folder")
    with group("searchgroup"):
        add_input_text("StudyFolderPath", label="", 
                       hint="Copy path to your folder here. Press Enter or Preview to view the log", 
                       width=520,
                       on_enter=True,
                       callback = set_folder,
                       callback_data = lambda:get_value("StudyFolderPath")
                       )
        add_same_line()
        add_button("ClearCurrentPath", label="Clear Path",
                   callback = clear_current_path,
                   width = 95,
                  )
    add_separator()
    
    
# setup right panel : history
with window("History",
            no_title_bar = True,
            autosize = False,
            no_resize=True,
            no_move=True,
            x_pos = 220,
            y_pos = 80,
            width = 660,
            height = 100,
            ):
    add_text("Previous Folders")
    add_same_line(spacing=432)
    add_button("ClearFolderHistory", label="Clear History",
                callback = clear_history,
                width = 95,
                show = False
                )
    # if paths are saved load them as buttons
    if len(APPDATA['paths'].keys()) != 0:
        # turn on the clear history button
        configure_item("ClearFolderHistory", show=True)
        for path in APPDATA['paths'].keys():
            add_button(f"{path}",
                        width = 627,
                        callback = set_folder,
                        callback_data= path
                        )

# setup right panel : content
with window("Main",
            no_title_bar = True,
            autosize = False,
            no_resize=True,
            no_move=True,
            x_pos = 220,
            y_pos = 170,
            width = 660,
            height = 350,
            ):
        
    add_separator()
    add_text("Preview Changes")
    with tab_bar("ModTabs"):
        with tab("Added"):
            add_table("AddedTable",
                        ["Study Group", "Study Number", "Description", "Date Created"],
                        height=282, width=627,
                        callback=table_action
                        )
        with tab("Removed"):
            add_table("RemovedTable",
                        ["Study Group", "Study Number", "Description", "Date Created"],
                        height=282, width=627,
                        callback=table_action
                        )
        with tab("Existing"):
            add_table("ExistingTable",
                        ["Study Group", "Study Number", "Description", "Date Created"],
                        height=282, width=627,
                        callback=table_action
                        )
        
# set tab colors
set_theme_item(mvGuiCol_Tab, 84, 84, 84, 220)
set_theme_item(mvGuiCol_TabHovered, 207, 209, 211, 204)
set_theme_item(mvGuiCol_TabActive, 140, 140, 140, 255)
        
# setup right panel : status bar
with window("StatusBar",
            no_title_bar = True,
            autosize = False,
            no_resize=True,
            no_move=True,
            x_pos = 220,
            y_pos = 520,
            width = 660,
            height = 80,
            ):
    add_progress_bar("ProgressBar", width=627)
    set_item_color("ProgressBar", color=(71,71,71), style=mvGuiCol_PlotHistogram)


#setup bottom panel : status bar
with window("VPNBar",
            no_title_bar = True,
            autosize= False, 
            no_resize=True,
            no_move=True,
            no_scrollbar= True,
            x_pos = 0,
            y_pos = get_main_window_size()[1]-80, 
            width = 220,
            height = 80,
            ):
    #add_image("RIOSLogo", value="resources\RIOSDevSmall.png", height=24)
    add_image("VPNStatusIcon",r"resources\12x12-idle.png")
    add_same_line(name="VPNsameline", spacing=10)
    add_text("RIOS VPN Status", default_value="Checking VPN Status...")
    connection = vpn_status()

set_style_frame_rounding(5)
set_style_window_rounding(0)
set_style_window_border_size(0)
set_style_button_text_align(0,.5)

#demo.show_demo()
#show_style_editor()
#show_documentation()
#show_debug()
#show_logger()


start_dearpygui()