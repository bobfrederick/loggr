# excelwriter.py
# https://openpyxl.readthedocs.io/en/stable/pandas.html

import os
import shutil
from openpyxl import load_workbook
import openpyxl
from openpyxl.styles import PatternFill
from openpyxl.workbook.workbook import Workbook
from openpyxl.worksheet.worksheet import Worksheet
import pandas as pd
from pandas import ExcelWriter
from zipfile import BadZipFile
from folderparse import walk, build_dict
import re
from shutil import copyfile
from dearpygui.core import *
from dearpygui.simple import *

def get_project_info(folder):
    """ Get the project name and number from the folder name and create a 
        filename for the excel log file. 

    Parameters
    ----------
    folder :  string, file-like path name to folder

    Returns
    -------
    tuple
        A tuple containing the project name, number, log file path as strings.
    """
    # parse the folder to get the project number and name. 
    project_string = os.path.basename(os.path.dirname(os.path.dirname(folder)))
    m = re.match("(\d{5})(\D+)",project_string)
    project_number = m.group(1)
    project_name = m.group(2).strip()
    # build output filename vased
    log_file = project_number+"_"+project_name+" - Study Log.xlsx"
    return (project_number,project_name, log_file)

def get_excel_file(folder):
    """ get the first sorted excel file in the root folder """
    matching_files = []
    files = os.listdir(folder)
    for file in files:
        if file.endswith('.xlsx'):
            matching_files.append(os.path.join(folder,file))
    #Sort the list to take the first entry
    #TODO need to make sure this sort matches the file system so behavior is predictable
    matching_files.sort()
    return matching_files

def copy_template_file(folder, filename):
    """ Copy the log excel template file into the project study folder"

    Parameters
    ----------
    folder :  string, file-like path name to folder
    filename : string, name of the template file. 
    """
    template = r"templates\XXXXX_Project Name - Study Log.xlsx"
    full_path_template = os.path.join(folder,template)
    full_path_project= os.path.join(folder,filename)
    # copy the file to the project folder.
    shutil.copyfile(template,full_path_project)
    # rename to match the project name. 
    #os.rename(full_path_template,full_path_project)

def insert_project_info_in_file(worksheet: Worksheet, project_number, project_name):
    """ Fills the project log with the project number and name

    Parameters
    ----------
    worksheet : a valid openpyxl Worksheet object. 
    project_number : string, a 5 digit project number.
    project_name : string, the name of the project.
    """
    if worksheet['D2'] == project_number and worksheet['E2'] == project_name:
        pass
    else:
        worksheet['D2'] = project_number
        worksheet['E2'] = project_name

def build_dataframe(folder_dict):
    """ Build a pandas dataframe from the folder structure """
    df = pd.DataFrame(folder_dict)
    return df

def simplify_dataframe(df_full):
    """ Simplifies the columns for the dataframe prep for excel export """
    df2 = df_full.drop(['path','date_modified'], axis=1)
    return df2

def dataframe_to_excel(filename,sheet_name, df, start_row, start_column, headers=False, index_col=False):
    """ Write a dataframe into excel at a specified position and return the workbook object.
    
    Parameters
    -----------
    filename : file-like path name to excel file
    sheet_name : string, sheet name in workbook.
    df : Pandas.DataFrame to write into excel.
    start_row : integer to specify at which row data should be inserted.
    start_column: integer to specify  at which column data should be inserted.
    **kwargs : keyword arguments to pass to the underlying pd.DataFrame.to_excel
        method: 
        <https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_excel.html>

    """
    try:
        workbook = load_workbook(filename)
    except BadZipFile as e:
        return "template file is corrupt, replace."
    try:
        writer = ExcelWriter(filename, engine='openpyxl')
    except PermissionError as e:
        return "template file is locked by another user."

    writer.book = workbook
    writer.sheets = dict((ws.title, ws) for ws in workbook.worksheets)
    
    ws = workbook[sheet_name]
    
    # clear previous cells value
    for row in ws.iter_rows(min_row=4, max_row=ws.max_row, max_col=4):
        for cell in row:
            cell.value = None

    # clear previous cells style
    for row in ws.iter_rows(min_row=4, max_row=ws.max_row, max_col=ws.max_column):
        for cell in row:
            cell.style = 'Normal'
 
    # write data to cells
    df.to_excel(writer,header=headers, index=index_col, sheet_name='Sheet1', startrow=start_row, startcol=start_column)
 
    writer.save()
    writer.close()
    return workbook

def initalize_worksheet(filepath: str, workbook: Workbook, sheet_name: str) -> Worksheet:
    """ Load worksheet object and clean any existing outlines out of file
        and return the worksheet. 

    Parameters
    ----------
    filepath : file-like path name to excel file.
    workbook : workbook object 
    sheet_name = string object representing the sheet to load. 
    """
    ws = workbook[sheet_name]
    # remove any old outlines by setting the outline level to 0 and all rows with ws.max_rows
    ws.row_dimensions.group(0,ws.max_row,outline_level=0)
    # save the file.         
    workbook.save(filepath)
    
    return ws

def find_row_groups_by_column_comparison(worksheet, row_min, col_max, row_max, only_values=True):
    """ Find where the outline group boundaries are by row using openpyxl's Worksheet.iter_rows. 
        Insert blank rows to separate the groups. We detect the changes by comparing the study_no column 
        to the study_group for inclusion. i.e. is study 200 in 101-200 if True this would mark the end of 
        the 101-200 grouping.By building a tuple to hold these row positions we can iterate through them later. 
        This provides separation of control from the group detection algorithm and the row/cell modifying functions.

    Parameters
    ----------
    worksheet : a valid openpyxl Worksheet object.
    row_min : int, smallest row index (1-based index).
    col_max : int,  largest column index (1-based index).
    row_max : int, largest row index (1-based index).
    only_values : bool, whether only cell values should be returned.

    Returns
    -------
    3 value tuple containing, 
        row_inserts: 1-based indicies in which to insert new rows.
        outline_levels : 1-based indicies in which to group the rows.
        last_group_value : string value for the last inserted row.
    """

    def last_row(worksheet):
        """ Get the last row of the worksheet with data not including None.
            This function is an alternative to the openpyxl Worksheet.max_row
            function which includes cells with the value == None.
            <https://openpyxl.readthedocs.io/en/stable/api/openpyxl.worksheet.worksheet.html?highlight=worksheet#openpyxl.worksheet.worksheet.Worksheet.max_row> 

            Parameters
            ----------
            worksheet : valid openpyxl.Worksheet object 
        """
        count = 0 
        for row in worksheet:
            if not all ([cell.value == None for cell in row]):
                count += 1
        return(count)

    count = 0
    total = 0
    outline_levels = []
    row_inserts = []
    lastrow = last_row(worksheet)

    for i, row in enumerate(worksheet.iter_rows(min_row=row_min, max_col=col_max, max_row=row_max, values_only=only_values)):
        if row[1] and row[1] in row[0]:
            count += 1
            total += 1
            # count 1 determines the begining of the group row.
            if count == 1:
                # first row insert
                position_a = i+row_min+len(row_inserts)
            # count 2 determines the completion of the group row.
            elif count == 2:
                position_b = i+row_min+count
                row_inserts.append((position_a,row[0]))
                if total == 2:
                    outline_levels.append((position_a+1,position_b-1))
                else:
                    outline_levels.append((position_a+1,position_b))
                count = 0
    
    # last group in row
    last_group_value = [row for row in worksheet.values if row[1] is not None][-1][0]
    row_inserts.append((position_a,last_group_value))

    # determine last outline end by the last row + the total number of previously added rows. 
    added_rows = len(row_inserts)
    outline_levels.append((position_a+1,lastrow+added_rows))
    row_boundaries = (row_inserts, outline_levels)
    return row_boundaries

def add_row_groups(filename, workbook, worksheet, row_boundaries):
    """ Inserts rows at each specified row index for bounds of the groups, then groups rows between to fold rows 
    
    Parameters
    ----------
    filename : file-like path name to excel file.
    worksheet : valid openpyxl Worksheet object.
    row_boundaries: iterable, ( insert_list , outline_list , last_group_value )
        -insert_list is an iterable [(int, str)], containing row indicies (1-based index) to insert rows, 
            and string values to insert.
        -outline_list is an iterable, [(starting_row, ending_row)], containing row indicies (1-based index),
            starting row and ending row to group. 
        -last_group_value is a string value for the last inserted row.
    """
    insert_list = row_boundaries[0]
    outline_list = row_boundaries[1]
    
    ## insert rows 
    for row in insert_list:
        worksheet.insert_rows(row[0])
        worksheet.cell(row[0],1,row[1])
        # loop through cells in row and apply color fill for outline rows.
        r = list(worksheet.iter_rows(min_row=row[0], max_col=worksheet.max_column, max_row=row[0]))
        for cell in r[0]:
            cell.fill = PatternFill("solid", fgColor="E7E6E6")

    # if there are any user defined cells in cols D->H move them up based on the number of inserted rows.
    top_row = outline_list[0][0]
    bottom_row = outline_list[-1][1]
    user_cells = worksheet[f'E{top_row}':f'J{bottom_row}']
    # check there are any values in the user defined cells.
    if any(list([[cell.value for cell in row if cell.value != None] for row in user_cells])):
        for i, row in enumerate(outline_list):
            #print(f"Moving row E{row[0]}:J{row[1]} up by {(i+1)*-1} rows and 0 columns")
            worksheet.move_range(f"E{row[0]}:J{row[1]}", rows=(i+1)*-1, cols=0)
    else:
        print("No user defined fields.")

    # insert outline levels
    worksheet.sheet_properties.outlinePr.summaryBelow = False
    for row in outline_list:
        worksheet.row_dimensions.group(row[0],row[1], hidden=False, outline_level=1)
    try:
        workbook.save(filename)
        workbook.close()
        return True
    except:
        return None

def update_progress_bar(value, message):
    """ update dearpygui progress bar if in app context"""
    try:
        set_value("ProgressBar", value)
        configure_item("ProgressBar", overlay= message)
    except:
        print(message)

def write_folder_data_to_excel(folderpath, data):
    """ Main function that runs all of the openpyxl helper functions for
        processing the data and setting up the excel file. 
    
    Parameters
    ----------
    folderpath : file-like path name to folder to be processed into excel file
    data : dictionary, data to write to the excel file. 

    """
    ""
    update_progress_bar(10, "Gathering project information...")
    project_info = get_project_info(folderpath)
    folder_files = get_excel_file(folderpath)   
    template_file = os.path.join(folderpath,project_info[2])
    
    # check that the template file exists:
    if template_file in folder_files:
        pass
    else:
        #create template file.
        update_progress_bar(25, "Creating a new template log in project folder...")
        copy_template_file(folderpath,project_info[2])
    
    update_progress_bar(35, "Building the data..." )
    df = build_dataframe(data)
    df2 = simplify_dataframe(df)

    # write the data to excel and clean up the template file if there's anything in it.
    update_progress_bar(50,"Writing the data into the log..." )
    book = dataframe_to_excel(template_file,"Sheet1",df2,3,0)
    if isinstance(book, openpyxl.Workbook):
        ws = initalize_worksheet(template_file,book,"Sheet1")
        # insert project nubmer and name into file if it isn't already.
        insert_project_info_in_file(ws,project_info[0],project_info[1])
    else:
        return book

    # calculate the row grouping
    update_progress_bar(75, "Finishing things up...")
    row_bounds = find_row_groups_by_column_comparison(ws, 4, 2, ws.max_row, True)
    
    # finally, roup the rows.
    result = add_row_groups(template_file, book, ws, row_bounds)

    if result:
        return True
    else:
        return None

if __name__ == "__main__":
    CURRENT_FOLDER = r"P:\18031 East Valley Road Residence\18031_0 Drawings\18031_5 Studies"
    dirs = list(walk(CURRENT_FOLDER, 2))
    data = build_dict(dirs) 
    write_folder_data_to_excel(CURRENT_FOLDER,data)