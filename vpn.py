# vpn.py
# https://stackoverflow.com/questions/25327294/how-to-list-all-the-ip-addresses-defined-on-a-windows-system-with-python#25328180

import subprocess
import re
import sched, time
import threading
from dearpygui.core import *
from dearpygui.simple import *
si = subprocess.STARTUPINFO()
si.dwFlags |= subprocess.STARTF_USESHOWWINDOW

def change_pic(item, image, parent, before):
    """ changes a an image in dearpygui by deleting and replacing it """
    if does_item_exist(item):
        delete_item(item)
        add_image(item, value = image, parent = parent, before = before)

s = sched.scheduler(time.time, time.sleep)

def check_vpn(sc):
    proc = str(subprocess.check_output("ipconfig",startupinfo=si))
    ip = re.findall(r"(?<=IPv4 Address. . . . . . . . . . . : )(\d+\.\d+\.\d+\.\d+)",proc)
    if "rios.com" in proc:
        value =  ip
        set_value("RIOS VPN Status", f"VPN Connected" )
        change_pic("VPNStatusIcon",r"resources\12x12-connected.png", "LogoBar", "VPNsameline")
        #configure_item("VPNStatusIcon", color=(24,188,156))
        #print(f"Connected | RIOS VPN | {ip[0]}")
    else:
        set_value("RIOS VPN Status", "VPN Disconnected")
        change_pic("VPNStatusIcon",r"resources\12x12-disconnected.png", "LogoBar", "VPNsameline")
        #configure_item("VPNStatusIcon", color=(231,76,60))
        #print("Disconnected | RIOS VPN")
        value = None
    s.enter(4, 1, check_vpn, (sc,))
    return value

def vpn_status():
    """ threaded run for vpn connection checking """
    s.enter(4, 1, check_vpn, (s,))
    x = threading.Thread(target=s.run, daemon=True)
    x.start()

if __name__ == "__main__":
    vpn_status()
    for i in range(100):
        print(i)
        time.sleep(2)