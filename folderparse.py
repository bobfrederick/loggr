# folder_parse.py

import os
from posixpath import basename
import re
from pathlib import Path
import time
from dearpygui.core import *
from dearpygui.simple import *
import pandas as pd
#from fileowner import get_current_user, get_file_owner

# main regex logic for parsing folders
FOLDER_GROUP = '(\d+-\d+)' # matches study group folder format "100-200" 
#TODO update to capture full folder names if they include underscores ie.e 240_New_Study_Bedroom
FOLDER_CHILD  = '(\d{3}_)' # matches study folder format: "230_Mud Room Updates"

class Progress():
    """ Generic class to set and get progress updates  """
    def __init__(self, id):
        if does_item_exist(id):
            self.value = None
        else:
            print(f"No id matching {id} found in dearpygui")
            return
        
    def set_value(self, val, msg):
        """ set progressbar value and message """
        set_value(id, val )
        configure_item(id, overlay=str(PROGRESS)+"Checking Paths")

    def get_value(self):
        return get_value(id)

# setup a progress bar using the id 
PROGRESS = Progress("ProgressBar")

# progress bar helper
def normalize(val):
    """ normalize a number between 0-100 to 0-1 """
    return ((1-0)/(1-0)*(val-100)+100)/100

def walk(path, depth):
    """Recursively list files and directories up to a certain depth"""
    depth -= 1
    #global PROGRESS
    with os.scandir(path) as p:
        """ try:
            PROGRESS.set_value(.45, "Scanning directories...")
        except:
            pass """
        for entry in p:
            # filter folders based on study folder template
            group_folder = re.match(FOLDER_GROUP,entry.name)
            child_folder = re.match(FOLDER_CHILD,entry.name)
            if entry.is_dir(follow_symlinks=False) and group_folder or child_folder:
                yield entry.path
                if depth > 0:
                    yield from walk(entry.path, depth)
    """ try:
        PROGRESS.set_value(1.0, "Finished processing directories")
    except:
        pass """

def sort_dict(dictionary, sort_key):
    """ sorts a dictionary using pandas """
    df = pd.DataFrame.from_dict(dictionary)
    df.sort_values(by=[sort_key])
    return df.to_dict('records')

def build_dict(dir_list):
    """ Build a dict to hold the directories and groups."""
    # progress bar stuff
    #global PROGRESS
    data = []
    counter = 0
    total = len(dir_list)
    # primary loop
    for dir in dir_list:
        path = Path(dir)
        # check that were only taking child folder paths not group paths. 
        if re.match(FOLDER_CHILD, os.path.basename(path)):
            data.append({"group" : os.path.basename(path.parent),
                         "path" : dir,
                         "study_no": os.path.basename(path).split("_")[0],
                         "study_name": os.path.basename(path).split("_")[1],
                         "date_created": time.strftime("%m-%d-%Y", time.gmtime(os.stat(path).st_ctime)),
                         "date_modified": time.strftime("%m-%d-%Y", time.gmtime(os.stat(path).st_mtime)),
                         #"current_owner": get_file_owner(path)
                         })
        counter += 1
        value = int((counter/total) * 100)
        """ try:
            PROGRESS.set_value(normalize(value),str(value)+" %")
        except:
            pass """
        
        # use pandas to sort the data by study_no

    return sort_dict(data, "study_no")

if __name__ == "__main__":
    filepath = r"P:\18031 East Valley Road Residence\18031_0 Drawings\18031_5 Studies"
    dirs = list(walk(filepath, 2))
    data = build_dict(dirs)
    