# fileowner.py
# uses pywin32 dependency
# http://timgolden.me.uk/python/win32_how_do_i/get-the-owner-of-a-file.html

import win32api
import win32con
import win32security

def get_current_user():
    name = win32api.GetUserNameEx (win32con.NameSamCompatible)
    return str(name).split("\\")[1]

def get_file_owner(filepath):
    filepath = str(filepath)
    sd = win32security.GetFileSecurity (filepath, win32security.OWNER_SECURITY_INFORMATION)
    owner_sid = sd.GetSecurityDescriptorOwner ()
    try:
        name, domain, type = win32security.LookupAccountSid (None, owner_sid)
        return name
    except:
        name = "n/a"
        return name

if __name__ == "__main__":
    filename = r"P:\18031 East Valley Road Residence\18031_0 Drawings\18031_5 Studies\101-200\107_Pricing Set Final Review Prep"
    print(get_current_user())
    print(get_file_owner(filename))